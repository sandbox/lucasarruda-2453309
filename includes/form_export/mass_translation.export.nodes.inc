<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_nodes($form, $form_state) {

  $step = $form_state['step'];
  $step_values = (isset($form_state['step_information'][$step]['stored_values'])) ? $form_state['step_information'][$step]['stored_values'] : NULL;

  $form['selection'] = array(
    '#title' => t('Select which nodes will be translated'),
    '#type' => 'radios',
    '#options' => array(
      'all' => t('All'),
      'select' => t('Select manually'),
    ),
    '#default_value' => isset($step_values['selection']) ? $step_values['selection'] : 'all',
  );

  // Build the table header.
  $header = array(
    'title' => array('data' => t('Title'), 'class' => 'node-title'),
    'type' => array('data' => t('Type'), 'class' => 'node-type'),
    'author' => array('data' => t('Author'), 'class' => 'node-author'),
    'status' => array('data' => t('Status'), 'class' => 'node-status'),
    'changed' => array('data' => t('Updated'), 'class' => 'node-changed active'),
    'op' => array('data' => t('Operations'), 'class' => 'column-op'),
  );

  // Get the node data.
  $nids = _mass_translation_get_node_list();
  $nodes = node_load_multiple($nids);

  // Build the rows.
  $options = array();
  $content_types = array();
  foreach ($nodes as $node) {

    $content_type = check_plain(node_type_get_name($node));
    $options[$node->nid] = array(
      'title' => array(
        'data' => array(
          '#type' => 'item',
          '#title' => $node->title,
        ),
        'class' => 'node-title',
      ),
      'type' => array(
        'data' => $content_type,
        'class' => 'node-type',
      ),
      'author' => user_load($node->uid)->name,
      'status' => $node->status ? t('published') : t('not published'),
      'changed' => format_date($node->changed, 'short'),
      'op' => array(
        'data' => l(t('view'), 'node/' . $node->nid),
        'class' => 'column-op',
      ),
      '#attributes' => array('class' => array($node->type)),
    );

    $content_types[$node->type] = $content_type;
  }

  // Selected nodes (default value).
  $selected_nodes = isset($step_values['nodetable']['nodes']) && is_array($step_values['nodetable']['nodes']) ? drupal_map_assoc(array_filter($step_values['nodetable']['nodes'])) : array();

  foreach ($selected_nodes as $nid => $selected) {
    $options[$nid]['#attributes']['class'][] = 'selected';
  }

  // Build the tableselect wrapper.
  $form['nodetable'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node selection'),
    '#description' => t('All nodes of translatable content types can be selected'),
    '#states' => array(
      'invisible' => array(
        // Hide tableselect if 'all' nodes radio button is checked.
        ':input[name="selection"]' => array('value' => 'all'),
      ),
    ),
  );

  // Node Filters.
  $form['nodetable']['controls'] = array(
    '#theme_wrappers' => array('container'),
    '#id' => 'mass-translation-node-filter-controls',
  );

  $form['nodetable']['controls']['node_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
  );

  $form['nodetable']['controls']['node_type'] = array(
    '#type' => 'select',
    '#title' => t('Content types'),
    '#options' => array('' => t('- All -')) + $content_types,
  );

  // Node table.
  // Drupal core issue: tableselect elements can't configure #sticky option of theme_table.
  // Solution: append the class "tableheader-processed" in the element
  $form['nodetable']['nodes'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $selected_nodes,
    '#empty' => t('No content available.'),
    '#attributes' => array('class' => array('mass-translation-export-form tableheader-processed')),
    '#prefix' => '<div class="node-table-wrapper"><div class="scroll-filler"></div><div class="node-table-inner-wrapper">',
    '#suffix' => '</div></div>',
  );

  $form['include_fields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Export fields settings from content-types'),
    '#default_value' => isset($step_values['include_fields']) ? $step_values['include_fields'] : TRUE,
  );

  $form['zip'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate ZIP file with content images and files'),
    '#default_value' => isset($step_values['zip']) ? $step_values['zip'] : TRUE,
  );

  return $form;
}

function mass_translation_export_form_nodes_submit($form, &$form_state) {
  $groups = &$form_state['step_information'][1]['stored_values']['translate_groups'];
  if ($form_state['values']['include_fields']) {
    $groups['fields'] = 'fields';
  }
  else {
    $groups['fields'] = '0';
  }

}
