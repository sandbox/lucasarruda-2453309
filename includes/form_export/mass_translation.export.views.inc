<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_views($form, $form_state) {

  $step = $form_state['step'];
  $step_values = (isset($form_state['step_information'][$step]['stored_values'])) ? $form_state['step_information'][$step]['stored_values'] : NULL;

  // Build the table header.
  $header = array(
    'name' => array('data' => t('View')),
    'op' => array('data' => t('Operations'), 'class' => 'mass-translation-operations'),
  );

  // Get the block data.
  $views = _mass_translation_get_view_list();

  // Build the rows.
  $options = array();
  foreach ($views as $view) {
    $options[$view['name']] = array(
      'name' => format_string('@name (@id)', array('@name' => $view['human_name'], '@id' => $view['name'])),
      'op' => array(
        'data' => l(t('edit'), 'admin/structure/views/view/' . $view['name'] . '/edit'),
        'class' => 'mass-translation-operations',
      ),
    );
  }

  // Selected views (default value)
  $selected_views = isset($step_values['views']) && is_array($step_values['views']) ? drupal_map_assoc(array_filter($step_values['views'])) : array();

  foreach ($selected_views as $key => $selected) {
    $options[$key]['#attributes']['class'][] = 'selected';
  }

  $form['views'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $selected_views,
    '#empty' => t('No content available.'),
    '#attributes' => array('class' => array('mass-translation-export-form')),
  );

  return $form;

}