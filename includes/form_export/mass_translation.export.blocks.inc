<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_blocks($form, $form_state) {

  $step = $form_state['step'];
  $step_values = (isset($form_state['step_information'][$step]['stored_values'])) ? $form_state['step_information'][$step]['stored_values'] : NULL;

  // Build the table header.
  $header = array(
    'title' => array('data' => t('Block')),
    'ready' => array('data' => t('Ready for translation'), 'class' => 'mass-translation-ready'),
    'op' => array('data' => t('Operations'), 'class' => 'mass-translation-operations'),
  );

  // Get the block data.
  $blocks = _mass_translation_get_block_list();
  $regions = system_region_list(variable_get('theme_default'));

  // Build the rows.
  $options = array();
  $disabled_blocks = array();
  foreach ($blocks as $block) {
    if ($block['module'] == 'block') {
      $key = $block['delta'];
      $is_translatable = $block['i18n_mode'] == I18N_MODE_LOCALIZE;

      // Block name.
      $region = isset($regions[$block['region']]) ? $regions[$block['region']] : t('None');
      $options[$key]['title']['data'] = array(
        '#type' => 'item',
        '#title' => check_plain($block['info']),
        '#description' => t('Region: @region-name', array('@region-name' => $region)),
      );

      // Block status (is ready for translation).
      if ($is_translatable) {
        $options[$key]['ready']['data'] = array(
          '#markup' => t('Yes'),
        );
      }
      else {
        $options[$key]['ready']['data'] = array(
          '#markup' => t('Not translatable'),
        );
        $disabled_blocks[] = $key;
      }
      $options[$key]['ready']['class'] = 'mass-translation-ready';

      $options[$key]['op'] = array(
        'data' => l(t('edit'), 'admin/structure/block/manage/' . $block['module'] . '/' . $block['delta'] . '/configure'),
        'class' => 'mass-translation-operations',
      );
    }
  }

  // Selected nodes (default value)
  $selected_blocks = isset($step_values['blocks']) && is_array($step_values['blocks']) ? drupal_map_assoc(array_filter($step_values['blocks'])) : array();

  foreach ($selected_blocks as $key => $selected) {
    $options[$key]['#attributes']['class'][] = 'selected';
  }

  $form['blocks'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $selected_blocks,
    '#empty' => t('No content available.'),
    '#attributes' => array('class' => array('mass-translation-export-form')),
  );

  foreach ($disabled_blocks as $key) {
    $form['blocks'][$key]['#disabled'] = TRUE;
  }

  return $form;
}
