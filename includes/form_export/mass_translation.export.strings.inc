<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_strings($form, $form_state) {

  $step = $form_state['step'];
  $step_values = (isset($form_state['step_information'][$step]['stored_values'])) ? $form_state['step_information'][$step]['stored_values'] : NULL;

  // Get active modules.
  $active_modules = module_list();
  $module_info = system_get_info('module');

  $options_modules = array();
  foreach ($active_modules as $module) {
    $options_modules[$module] = format_string('@name (@id)', array('@name' => $module_info[$module]['name'], '@id' => $module));
  }
  asort($options_modules);

  $form['module_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Modules'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Only active modules are listed.'),
  );

  $form['module_list']['module'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select modules to be translated'),
    '#options' => $options_modules,
    '#default_value' => isset($step_values['module_list']['module']) ? $step_values['module_list']['module'] : array(),
  );

  // Get active themes.
  $active_themes = list_themes();

  $options_themes = array();
  foreach ($active_themes as $id => $theme) {
    if ($theme->status == 1) {
      $options_themes[$id] = format_string('@name (@id)', array('@name' => $theme->info['name'], '@id' => $id));
    }
  }

  $form['theme_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Themes'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Only active themes are listed.'),
  );

  $form['theme_list']['theme'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select themes to be translated'),
    '#options' => $options_themes,
    '#default_value' => isset($step_values['theme_list']['theme']) ? $step_values['theme_list']['theme'] : array(),
  );
  
  $form['custom_strings'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom Strings'),
    '#description' => t('Add custom strings to be translated.'),
    '#default_value' => isset($step_values['custom_strings']) ? $step_values['custom_strings'] : '',
  );

  return $form;
}
