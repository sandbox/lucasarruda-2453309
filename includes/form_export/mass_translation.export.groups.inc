<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_groups($form, $form_state) {

  $step = $form_state['step'];
  $step_values = (isset($form_state['step_information'][$step]['stored_values'])) ? $form_state['step_information'][$step]['stored_values'] : NULL;

  $form['description'] = array(
    '#markup' => t('Select content structure groups to be translated'),
  );

  $groups = array(
    'nodes' => t('Nodes'),
    'blocks' => t('Blocks'),
    'menus' => t('Menus'),
    'views' => t('Views'),
    'strings' => t('Strings'),
  );

  foreach ($groups as $key => $group) {
    $default_values[] = $key;
  }

  // Remove fields group in the first step.
  unset($step_values['translate_groups']['fields']);

  $form['translate_groups'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Translation Groups'),
    '#required' => TRUE,
    '#default_value' => isset($step_values['translate_groups']) ? $step_values['translate_groups'] : $default_values,
    '#options' => $groups,
  );

  return $form;
}


function mass_translation_export_form_groups_submit($form, &$form_state) {
  $groups = &$form_state['values']['translate_groups'];

  unset($groups['fields']);
  if ($groups['nodes']) {
    _array_push_to_position($groups, '0', 1, 'fields');
  }
  else {
    $groups['fields'] = '0';
  }
}

