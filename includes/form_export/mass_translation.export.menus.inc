<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_menus($form, $form_state) {

  $step = $form_state['step'];
  $step_values = (isset($form_state['step_information'][$step]['stored_values'])) ? $form_state['step_information'][$step]['stored_values'] : NULL;

  // Build the table header.
  $header = array(
    'title' => array('data' => t('Menu')),
    'ready' => array('data' => t('Ready for translation'), 'class' => 'mass-translation-ready'),
    'op' => array('data' => t('Operations'), 'class' => 'mass-translation-operations'),
  );

  // Get the menu data.
  $menus = db_select('menu_custom', 'm')->fields('m')->orderBy('title')->execute();

  // Build the rows.
  $options = array();
  $disabled_menus = array();
  while ($menu = $menus->fetchAssoc()) {
    $key = $menu['menu_name'];
    $is_translatable = $menu['i18n_mode'] == I18N_MODE_MULTIPLE && $menu['language'] == LANGUAGE_NONE;
    $is_lang_fixed = $menu['language'] != LANGUAGE_NONE;

    // Menu title.
    $options[$key]['title']['data'] = array(
      '#type' => 'item',
      '#title' => check_plain($menu['title']),
      '#description' => $menu['description'],
    );

    // Menu status (is ready for translation).
    if ($is_translatable) {
      $options[$key]['ready']['data'] = array(
        '#markup' => t('Yes'),
      );
    }
    else {
      if ($is_lang_fixed) {
        $options[$key]['ready']['data'] = array(
          '#markup' => t('Fixed language (@language)', array('@language' => $menu['language'])),
        );
      }
      else {
        $options[$key]['ready']['data'] = array(
          '#markup' => t('Not translatable'),
        );
      }
      $disabled_menus[] = $key;
    }
    $options[$key]['ready']['class'] = 'mass-translation-ready';

    $options[$key]['op'] = array(
      'data' => l(t('edit'), 'admin/structure/menu/manage/' . $menu['menu_name'] . '/edit'),
      'class' => 'mass-translation-operations',
    );

  }

  // Selected nodes (default value)
  $selected_menus = isset($step_values['menus']) && is_array($step_values['menus']) ? drupal_map_assoc(array_filter($step_values['menus'])) : array();

  foreach ($selected_menus as $key => $selected) {
    $options[$key]['#attributes']['class'][] = 'selected';
  }

  $form['menus'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $selected_menus,
    '#empty' => t('No content available.'),
    '#attributes' => array('class' => array('mass-translation-export-form')),
  );

  foreach ($disabled_menus as $key) {
    $form['menus'][$key]['#disabled'] = TRUE;
  }

  return $form;
}
