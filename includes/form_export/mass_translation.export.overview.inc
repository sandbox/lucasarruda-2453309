<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_overview($form, $form_state) {

  // Build the table header.
  $header = array(
    'group' => array('data' => t('Translation group')),
    'count' => array('data' => t('Number of elements')),
  );

  $steps = $form_state['step_information'];
  $groups = array_filter($steps[1]['stored_values']['translate_groups']);

  $rows = array();
  $count_total = 0;
  foreach ($groups as $group) {
    $group_title = '';
    $count_string = '';
    $count = 0;
    switch ($group) {
      case 'nodes':
        if ($steps[2]['stored_values']['selection'] == 'all') {
          $count = count($steps[2]['stored_values']['nodetable']['nodes']);
        }
        else {
          $count = count(array_filter($steps[2]['stored_values']['nodetable']['nodes']));
        }
        $group_title = t('Nodes');
        $count_string = format_plural($count, '1 node', '@count nodes');
        break;
      case 'fields':
        $count = count(array_filter($steps[3]['stored_values']['fields']));
        $group_title = t('Fields');
        $count_string = format_plural($count, '1 field', '@count fields');
        break;
      case 'blocks':
        $count = count(array_filter($steps[4]['stored_values']['blocks']));
        $group_title = t('Blocks');
        $count_string = format_plural($count, '1 block', '@count blocks');
        break;
      case 'menus':
        $count = count(array_filter($steps[5]['stored_values']['menus']));
        $group_title = t('Menus');
        $count_string = format_plural($count, '1 menu', '@count menus');
        break;
      case 'views':
        $count = count(array_filter($steps[6]['stored_values']['views']));
        $group_title = t('Views');
        $count_string = format_plural($count, '1 view', '@count views');
        break;
      case 'strings':
        $module_count = count(array_filter($steps[7]['stored_values']['module_list']['module']));
        $theme_count = count(array_filter($steps[7]['stored_values']['theme_list']['theme']));
        $custom_strings = explode("\r\n", $steps[7]['stored_values']['custom_strings']);
        $custom_count = count(array_filter($custom_strings));
        $count = $module_count + $theme_count + $custom_count;
        $group_title = t('Strings');
        $count_string_mod = format_plural($module_count, '1 module', '@count modules');
        $count_string_thm = format_plural($theme_count, '1 theme', '@count themes');
        $count_string_custom = format_plural($custom_count, '1 custom string', '@count custom strings');
        $count_string = format_string('@modules, @themes, @custom_strings', array('@modules' => $count_string_mod,
          '@themes' => $count_string_thm, '@custom_strings' => $count_string_custom));
        break;
    }

    // Build row data.
    $rows[] = array(
      'group' => array('data' => $group_title),
      'count' => array('data' => $count_string),
    );

    $count_total += $count;
  }

  $form['overview'] = array(
    '#markup' => theme('table', array(
      'header' => $header,
      'rows' => $rows,
    )),
  );

  // Disable Finish button if there is no content selected
  if ($count_total == 0) {
    $form['next']['#disabled'] = TRUE;
  }

  // Check if export has been completed.
  $export_complete = FALSE;
  if (isset($steps[8]['export_complete'])) {
    $export_complete = $steps[8]['export_complete'];
  }

  if ($export_complete) {

    $links = array();

    // XML download link
    if (isset($steps[8]['exported_xml'])) {
      $links['exported_xml'] = array(
        'title' => t('Download XML file'),
        'href' => $steps[8]['exported_xml'],
      );
    }

    // ZIP download link
    if (isset($steps[8]['exported_zip'])) {
      $links['exported_zip'] = array(
        'title' => t('Download ZIP file'),
        'href' => $steps[8]['exported_zip'],
      );
    }

    $form['export_results'] = array(
      '#markup' => theme('links', array('links' => $links)),
    );
  }

  return $form;
}


function mass_translation_export_form_overview_submit($form, &$form_state) {
  // Export Process XML/ZIP files.
  $export_results = _mass_translation_create_xml_file($form_state['step_information']);

  $form_state['step_information'][8]['export_complete'] = TRUE;
  if (isset($export_results['xml_file'])) {
    $form_state['step_information'][8]['exported_xml'] = $export_results['xml_file'];
  }
  if (isset($export_results['zip_file'])) {
    $form_state['step_information'][8]['exported_zip'] = $export_results['zip_file'];
  }

  // Set success message.
  drupal_set_message(t('Mass Translation exported the selected contents. Download links below.'));
  watchdog('mass_translation', 'Mass Translation content export completed.', array('!list' => theme('item_list', array('items' => $export_results))), WATCHDOG_ERROR);

}

/**
 * Implements _mass_translation_create_xml_file().
 * Generate XML file containing translatable contents
 * @param $steps
 *   steps information and user input from export form.
 * @return <string> XML file absolute url.
 */
function _mass_translation_create_xml_file($steps) {

  module_load_include('inc', 'mass_translation', 'includes/plugins/mass_translation.plugin');
  module_load_include('inc', 'mass_translation', 'includes/plugins/mass_translation.plugin.node');
  module_load_include('inc', 'mass_translation', 'includes/plugins/mass_translation.plugin.field');
  module_load_include('inc', 'mass_translation', 'includes/plugins/mass_translation.plugin.block');
  module_load_include('inc', 'mass_translation', 'includes/plugins/mass_translation.plugin.menu');
  module_load_include('inc', 'mass_translation', 'includes/plugins/mass_translation.plugin.view');
  module_load_include('inc', 'mass_translation', 'includes/plugins/mass_translation.plugin.string');

  $groups = array_filter($steps[1]['stored_values']['translate_groups']);

  $generate_zip = FALSE;
  if (isset($steps[2]['stored_values']['zip']) && $steps[2]['stored_values']['zip']) {
    $generate_zip = $steps[2]['stored_values']['zip'];
    $files = array();
  }

  $contents = array();
  foreach ($groups as $group) {
    switch ($group) {
      case 'nodes':
        if ($steps[2]['stored_values']['selection'] == 'all') {
          $count = count($steps[2]['stored_values']['nodetable']['nodes']);
          $nodes_selected = array_keys($steps[2]['stored_values']['nodetable']['nodes']);
        }
        else {
          $nodes_selected = array_filter($steps[2]['stored_values']['nodetable']['nodes']);
        }
        sort($nodes_selected);
        foreach ($nodes_selected as $nid) {
          $obj = new MassTranslationContentNode($nid);
          if ($translate_obj = $obj->getData()) {
            $contents['nodes'][]['node'] = $translate_obj;
            if ($generate_zip) {
              $files[] = $obj->getFiles();
            }
          }
        }
        break;
      case 'fields':
        $fields_selected = array_filter($steps[3]['stored_values']['fields']);
        foreach ($fields_selected as $field_id) {
          $obj = new MassTranslationContentField($field_id);
          if ($translate_obj = $obj->getData()) {
            $contents['fields'][]['field'] = $translate_obj;
          }
        }
        break;
      case 'blocks':
        $blocks_selected = array_filter($steps[4]['stored_values']['blocks']);
        foreach ($blocks_selected as $block_id) {
          $obj = new MassTranslationContentBlock($block_id);
          if ($translate_obj = $obj->getData()) {
            $contents['blocks'][]['block'] = $translate_obj;
          }
        }
        break;
      case 'menus':
        $menus_selected = array_filter($steps[5]['stored_values']['menus']);
        foreach ($menus_selected as $menu_id) {
          $obj = new MassTranslationContentMenu($menu_id);
          if ($translate_obj = $obj->getData()) {
            $contents['menus'][]['menu'] = $translate_obj;
          }
        }
        break;
      case 'views':
        $views_selected = array_filter($steps[6]['stored_values']['views']);
        foreach ($views_selected as $view_id) {
          $obj = new MassTranslationContentView($view_id);
          if ($translate_obj = $obj->getData()) {
            $contents['views'][]['view'] = $translate_obj;
          }
        }
        break;
      case 'strings':
        $strings_selected_module = array_filter($steps[7]['stored_values']['module_list']['module']);
        foreach ($strings_selected_module as $module) {
          $obj = new MassTranslationContentString($module, 'module');
          if ($translate_obj = $obj->getData()) {
            $contents['strings'][] = $translate_obj;
          }
        }
        $strings_selected_theme = array_filter($steps[7]['stored_values']['theme_list']['theme']);
        foreach ($strings_selected_theme as $theme) {
          $obj = new MassTranslationContentString($theme, 'theme');
          if ($translate_obj = $obj->getData()) {
            $contents['strings'][] = $translate_obj;
          }
        }
        $custom_strings = array_filter(explode("\r\n", $steps[7]['stored_values']['custom_strings']));
        foreach ($custom_strings as $custom_string) {
          $obj = new MassTranslationContentString($custom_string, 'custom');
          if ($translate_obj = $obj->getData()) {
            $contents['strings'][] = $translate_obj;
          }
        }
        break;
    }

  }

  // Generate XML file content.
  $xml = _mass_translation_xml_array_to_xml('contents', $contents);

  // Prepare directory.
  $timestamp = REQUEST_TIME;
  $directory = _mass_translation_prepare_directory();
  $file_uri = $directory . "/mass_translation_content[{$timestamp}].xml";

  // Create XML fle.
  file_unmanaged_save_data($xml, $file_uri);

  // Get XML file URL.
  $output['xml_file'] = file_create_url($file_uri);

  if ($generate_zip && count($files) && ($zip_file = _mass_translation_create_zip_file($files, $timestamp))) {
    if ($zip_file) {
      $output['zip_file'] = $zip_file;
    }
  }

  return $output;
}
