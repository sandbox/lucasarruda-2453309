<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function mass_translation_export_form_fields($form, $form_state) {

  $step = $form_state['step'];
  $step_values = (isset($form_state['step_information'][$step]['stored_values'])) ? $form_state['step_information'][$step]['stored_values'] : NULL;

  // Build the table header.
  $header = array(
    'name' => array('data' => t('Field')),
    'type' => array('data' => t('Type')),
    'types' => array('data' => t('Used in')),
  );

  // Get the block data.
  $fields = _mass_translation_get_field_list();

  // Build the rows.
  $options = array();

  foreach ($fields as $field) {

    $options[$field['name']] = array(
      'name' => format_string('@name (@id)', array('@name' => $field['label'], '@id' => $field['name'])),
      'type' => $field['type'],
      'types' => implode(', ', $field['content_types']),
    );

  }

  // Selected fields (default value)
  $selected_fields = isset($step_values['fields']) && is_array($step_values['fields']) ? drupal_map_assoc(array_filter($step_values['fields'])) : array();

  foreach ($selected_fields as $key => $selected) {
    $options[$key]['#attributes']['class'][] = 'selected';
  }

  $form['fields'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $selected_fields,
    '#empty' => t('No content available.'),
    '#attributes' => array('class' => array('mass-translation-export-form')),
  );

  return $form;

}