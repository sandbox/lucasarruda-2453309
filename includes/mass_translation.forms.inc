<?php
/**
 * @file
 * Administration pages for Mass Translation.
 */

function _mass_translation_export_form_steps() {
  return array(
    1 => array(
      'id' => 'groups',
      'title' => t('Export XML: Translation Groups'),
      'form' => 'mass_translation_export_form_groups',
      'file' => 'includes/form_export/mass_translation.export.groups',
      'btn_next' => t('Start'),
    ),
    2 => array(
      'id' => 'nodes',
      'title' => t('Export XML: Node selection (Step @step)'),
      'form' => 'mass_translation_export_form_nodes',
      'file' => 'includes/form_export/mass_translation.export.nodes',
      'btn_prev' => t('Back'),
      'btn_next' => t('Next'),
    ),
    3 => array(
      'id' => 'fields',
      'title' => t('Export XML: Fields selection (Step @step)'),
      'form' => 'mass_translation_export_form_fields',
      'file' => 'includes/form_export/mass_translation.export.fields',
      'btn_prev' => t('Back'),
      'btn_next' => t('Next'),
    ),
    4 => array(
      'id' => 'blocks',
      'title' => t('Export XML: Block selection (Step @step)'),
      'form' => 'mass_translation_export_form_blocks',
      'file' => 'includes/form_export/mass_translation.export.blocks',
      'btn_prev' => t('Back'),
      'btn_next' => t('Next'),
      'btn_refresh' => TRUE,
    ),
    5 => array(
      'id' => 'menus',
      'title' => t('Export XML: Menu selection (Step @step)'),
      'form' => 'mass_translation_export_form_menus',
      'file' => 'includes/form_export/mass_translation.export.menus',
      'btn_prev' => t('Back'),
      'btn_next' => t('Next'),
      'btn_refresh' => TRUE,
    ),
    6 => array(
      'id' => 'views',
      'title' => t('Export XML: View selection (Step @step)'),
      'form' => 'mass_translation_export_form_views',
      'file' => 'includes/form_export/mass_translation.export.views',
      'btn_prev' => t('Back'),
      'btn_next' => t('Next'),
    ),
    7 => array(
      'id' => 'strings',
      'title' => t('Export XML: String selection (Step @step)'),
      'form' => 'mass_translation_export_form_strings',
      'file' => 'includes/form_export/mass_translation.export.strings',
      'btn_prev' => t('Back'),
      'btn_next' => t('Next'),
    ),
    8 => array(
      'id' => 'overview',
      'title' => t('Export XML: Translation Overview'),
      'form' => 'mass_translation_export_form_overview',
      'file' => 'includes/form_export/mass_translation.export.overview',
      'btn_prev' => t('Back'),
      'btn_next' => t('Finish'),
    ),
  );
}

/**
 * Form constructor for the export XML file form.
 *
 * @see mass_translation_export_form_validate()
 *
 * @ingroup mass_translation_export_form
 */
function mass_translation_export_form($form, $form_state) {

  $form = array();
  $form['#tree'] = TRUE;

  // Append javascript and css files
  $module_path = drupal_get_path('module', 'mass_translation');
  $form['#attached'] = array(
    'css' => array($module_path . '/includes/mass_translation.admin.css'),
    'js' => array($module_path . '/includes/mass_translation.admin.js'),
  );

  // Initialize steps
  if (!isset($form_state['step']) || empty($form_state['step'])) {
    $form_state['step'] = 1;
    $form_state['step_count'] = 0;

    // Get step information
    $form_state['step_information'] = _mass_translation_export_form_steps();
  }
  $step = &$form_state['step'];
  $step_count = $form_state['step_count'];
  $step_info = $form_state['step_information'][$step];

  // Set form title
  drupal_set_title(format_string($step_info['title'], array('@step' => $step_count)));

  // Append refresh button
  if (isset($step_info['btn_refresh']) && $step_info['btn_refresh']) {
    $form['refresh'] = array(
      '#type' => 'submit',
      '#value' => t('Refresh'),
      '#validate' => array(),
      '#submit' => array('mass_translation_export_form_refresh_submit'),
    );
  }

  // Include step file and call function to build current step's form
  form_load_include($form_state, 'inc', 'mass_translation', $step_info['file']);
  $form = $step_info['form']($form, $form_state);

  // Check if export has been completed.
  $export_complete = FALSE;
  if (isset($form_state['step_information'][8]['export_complete'])) {
    $export_complete = $form_state['step_information'][8]['export_complete'];
  }

  // Show the 'previous' button if appropriate. Note that #submit is set to
  // a special submit handler, and that we use #limit_validation_errors to
  // skip all complaints about validation when using the back button. The
  // values entered will be discarded, but they will not be validated, which
  // would be annoying in a "back" button.
  if ($step > 1 && !$export_complete) {
    $form['prev'] = array(
      '#type' => 'submit',
      '#value' => $step_info['btn_prev'],
      '#name' => 'prev',
      '#validate' => array(),
      '#submit' => array('mass_translation_export_form_previous_submit'),
    );
  }

  // Show the Next button
  if (!$export_complete) {
    // Get previous set Next Button and unsets it.
    $btn_next = isset($form['next']) ? $form['next'] : array();
    unset($form['next']);

    $form['next'] = array(
      '#type' => 'submit',
      '#value' => $step_info['btn_next'],
      '#name' => 'next',
      '#submit' => array('mass_translation_export_form_next_submit'),
    ) + $btn_next;
  }

  if (isset($form['next'])) {
    // Bind validate functions
    $form['next']['#validate'] = array();
    // Include each validation function defined for the different steps.
    // First, look for functions that match the form_id_validate naming convention.
    if (function_exists($step_info['form'] . '_validate')) {
      $form['next']['#validate'] = array($step_info['form'] . '_validate');
    }
    // Next, merge in any other validate functions defined by child form.
    if (isset($form['#validate'])) {
      $form['next']['#validate'] = array_merge($form['next']['#validate'], $form['#validate']);
      unset($form['#validate']);
    }

    // Bind submit functions
    // Look for functions that match the form_id_submit naming convention.
    if (function_exists($step_info['form'] . '_submit')) {
      $form['next']['#submit'] = array_merge(array($step_info['form'] . '_submit'), $form['next']['#submit']);
    }
    // Next, merge in any other submit functions defined by child form.
    if (isset($form['#submit'])) {
      // It's important to merge in the form-specific handlers first, before
      // mass_translation_export_form_next_submit clears $form_state['values].
      $form['next']['#submit'] = array_merge($form['#submit'], $form['next']['#submit']);
      unset($form['#submit']);
    }
  }

  return $form;
}


/**
 * Submit handler for the 'prev' button.
 * - Saves away $form_state['values']
 * - Decrement the step count.
 * - Replace $form_state['values'] from the last time we were at this page.
 * - Force form rebuild.
 *
 * @ingroup mass_translation_export_form
 */
function mass_translation_export_form_previous_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $step_count = &$form_state['step_count'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  $form_steps = $form_state['step_information'];

  if ($current_step > 1) {
    $groups = $form_state['step_information'][1]['stored_values']['translate_groups'];
    foreach ($groups as $group) {
      $current_step--;
      if ($form_steps[$current_step]['id'] == 'groups' || $groups[$form_steps[$current_step]['id']] != '0') {
        break;
      }
    }
    $step_count--;
    $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
  }

  // Force rebuild with next step.
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the 'next' button.
 * - Saves away $form_state['values']
 * - Increments the step count.
 * - Replace $form_state['values'] from the last time we were at this page
 *   or with array() if we haven't been here before.
 * - Force form rebuild.
 *
 * @ingroup mass_translation_export_form
 */
function mass_translation_export_form_next_submit($form, &$form_state) {

  $current_step = &$form_state['step'];
  $step_count = &$form_state['step_count'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  $form_steps = _mass_translation_export_form_steps();

  if ($current_step < count($form_state['step_information'])) {
    $groups = $form_state['step_information'][1]['stored_values']['translate_groups'];

    foreach ($groups as $group) {
      $current_step++;
      if ($form_steps[$current_step]['id'] == 'overview' || $groups[$form_steps[$current_step]['id']] != '0') {
        break;
      }
    }
    $step_count++;
    if (!empty($form_state['step_information'][$current_step]['stored_values'])) {
      $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
    }
    else {
      $form_state['values'] = array();
    }
  }

  // Force rebuild with next step.
  $form_state['rebuild'] = TRUE;
}



/**
 * Submit handler for the 'refresh' button.
 * - Saves away $form_state['values']
 * - Force form rebuild.
 *
 * @ingroup mass_translation_export_form
 */
function mass_translation_export_form_refresh_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  // Force rebuild with next step.
  $form_state['rebuild'] = TRUE;
}


/**
 * @file
 * Import xml form for Mass Translation.
 */

function mass_translation_import_form($form, $form_state) {


  // Get import status (preview | finished)
  if (!isset($form_state['import_process']['status'])) {
    $form_state['import_process']['status'] = 'ready';
  }

  switch ($form_state['import_process']['status']) {
    case 'ready':
    case 'preview':
      // Set languages select list.
      $form['target_language'] = array(
        '#type' => 'select',
        '#title' => t('Select the language to apply the translations.'),
        '#description' => t('Only active languages are listed.'),
        '#options' => locale_language_list('name'),
        '#required' => TRUE,
      );

      $form['xml_upload'] = array(
        '#type' => 'managed_file',
        '#title' => t('Upload XML'),
        '#descripion' => t('Only XML files are allowed.'),
        '#upload_validators'  => array('file_validate_extensions' => array('xml')),
        '#required' => TRUE,
      );

      $form['zip_upload'] = array(
        '#type' => 'managed_file',
        '#title' => t('Upload Zip'),
        '#descripion' => t('Only Zip files are allowed.'),
        '#upload_validators'  => array('file_validate_extensions' => array('zip')),
      );

      if ($form_state['import_process']['status'] == 'preview') {

        $status = $form_state['import_process']['report'];

        // Build the table header.
        $header = array(
          'group' => array('data' => t('Translation group')),
          'count' => array('data' => t('Number of elements')),
        );

        $form['overview'] = array(
          '#prefix' => format_string('<label>@title</label>', array('@title' => t('Preview'))),
          '#markup' => theme('table', array(
            'header' => $header,
            'rows' => $status,
          )),
        );
      }

      $form['preview'] = array(
        '#type' => 'submit',
        '#value' => t('Preview'),
        '#submit' => array('mass_translation_import_form_preview'),
      );

      $form['import'] = array(
        '#type' => 'submit',
        '#value' => t('Import'),
      );

      break;

    case 'finished':

      // Build the table header.
      $header = array(
        'group' => array('data' => t('Translation group')),
        'successs' => array('data' => t('Success')),
        'failed' => array('data' => t('Failed')),
      );

      $rows = array();
      foreach ($form_state['import_process']['report'] as $group_name => $group) {
        $success = $failed = 0;
        switch ($group_name) {
          case 'nodes':
            $group_title = t('Nodes');
            $success = $group['success'];
            $failed = $group['failed'];
            $field_error = $group['field_error'];
            $success = format_plural($success, '1 node', '@count nodes');
            $failed = format_plural($failed, '1 node', '@count nodes');
            if ($field_error) {
              $success = t('@success, with field errors', array('@success' => $success));
            }
            break;

          case 'fields':
            $group_title = t('Fields');
            foreach ($group as $field_id => $field_status) {
              if ($field_status['skips'] == 0) {
                $success++;
              }
              else {
                $failed++;
              }
            }
            $success = format_plural($success, '1 field', '@count fields');
            $failed = format_plural($failed, '1 field', '@count fields');
            break;

          case 'blocks':
            $group_title = t('Blocks');
            foreach ($group as $block_delta => $block_status) {
              if ($block_status['skips'] == 0) {
                $success++;
              }
              else {
                $failed++;
              }
            }
            $success = format_plural($success, '1 block', '@count blocks');
            $failed = format_plural($failed, '1 block', '@count blocks');
            break;

          case 'menus':
            $group_title = t('Menus');
            $success_menu = format_plural($group['menu_success'], '1 menu', '@count menus');
            $success_item = format_plural($group['item_success'], '1 item', '@count items');
            $error_menu = format_plural($group['menu_error'], '1 menu', '@count menus');
            $error_item = format_plural($group['item_error'], '1 item', '@count items');
            $success = format_string('@menus (@items)', array('@menus' => $success_menu, '@items' => $success_item));
            $failed = format_string('@menus (@items)', array('@menus' => $error_menu, '@items' => $error_item));
            break;

          case 'views':
            $group_title = t('Views');
            foreach ($group as $view_id => $view_status) {
              if ($view_status['skips'] == 0) {
                $success++;
              }
              else {
                $failed++;
              }
            }
            $success = format_plural($success, '1 view', '@count views');
            $failed = format_plural($failed, '1 view', '@count views');
            break;

          case 'strings':
            $group_title = t('Strings');
            $success = $group['additions'] + $group['updates'];
            $failed = $group['skips'];
            $success = format_plural($success, '1 string', '@count strings');
            $failed = format_plural($failed, '1 string', '@count strings');
            break;

        }
        $rows[$group_name] = array(
          'group' => array('data' => $group_title),
          'successs' => array('data' => $success),
          'failed' => array('data' => $failed),
        );
      }

      $form['overview'] = array(
        '#markup' => theme('table', array(
          'header' => $header,
          'rows' => $rows,
        )),
      );
      break;
  }


  return $form;
}

function mass_translation_import_form_submit($form, &$form_state) {

  $langcode = $form_state['values']['target_language'];
  $xml_upload = $form_state['values']['xml_upload'];
  $zip_upload = $form_state['values']['zip_upload'];

  if ($xml_upload && $xml_file = file_load($xml_upload)) {

    libxml_use_internal_errors(TRUE);
    $xml_content = file_get_contents($xml_file->uri);
    $xml = simplexml_load_string($xml_content, 'SimpleXMLElement', LIBXML_NOCDATA);
    if ($xml === FALSE) {
      // Set error messages for invalid XML format.
      drupal_set_message(t('Unable to parse XML file.'), 'error');
      foreach (libxml_get_errors() as $error) {
        drupal_set_message(t('Error (line @line-number): @message', array('@line-number' => $error->line, '@message' => $error->message)), 'error');
      }
      libxml_clear_errors();
    }
    else {

      $dom = dom_import_simplexml($xml)->ownerDocument;
      $module_path = drupal_get_path('module', 'mass_translation');
      $xsd_path = drupal_realpath($module_path . '/includes/mass_translation_xml_schema.xsd');

      if (!$dom->schemaValidate($xsd_path)) {
        // Set error messages for invalid XML schema.
        drupal_set_message(t('XML do not match with XSD schema.'), 'error');
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
          $message = t('Error @errcode (level @level, line @line): @message', array(
            '@errcode' => $error->code,
            '@level' => $error->level,
            '@message' => trim($error->message),
            '@line' => $error->line,
          ));
          drupal_set_message($message, 'error');
        }
        libxml_clear_errors();
      }
      else {
        $status = array();
        foreach ($xml as $group => $elements) {
          switch ($group) {
            case 'nodes':
              $status['nodes'] = _mass_translation_translate_nodes($elements->node, $langcode, $zip_upload);
              break;
            case 'fields':
              $status['fields'] = _mass_translation_translate_fields($elements->field, $langcode);
              break;
            case 'blocks':
              $status['blocks'] = _mass_translation_translate_blocks($elements->block, $langcode);
              break;
            case 'menus':
              $status['menus'] = _mass_translation_translate_menus($xml->menus->menu, $langcode);
              break;
            case 'views':
              $status['views'] = _mass_translation_translate_views($elements->view, $langcode);
              break;
            case 'strings':
              $status['strings'] = _mass_translation_translate_strings($elements->string, $langcode);
              break;
          }
        }

        // Set success message and log the final status in watchdog.
        $languages = language_list();
        drupal_set_message(t('Translated XML processed for language "@language". Check Drupal log to get detailed information.', array('@language' => $languages[$langcode]->name)));
        watchdog('mass_translation', 'Translated XML processed for language "@language". Check the raw report: <br /><pre>@status</pre>', array('@language' => $langcode, '@status' => print_r($status, TRUE)), WATCHDOG_INFO);

        // Clear the cached pages and blocks.
        cache_clear_all();

        // Rebuild menu.
        menu_rebuild();

        // Set final status (to be rendered in the form).
        $form_state['import_process']['status'] = 'finished';
        $form_state['import_process']['report'] = $status;

        // Force rebuild with next step.
        $form_state['rebuild'] = TRUE;
      }
    }
  }
  else {
    drupal_set_message(t('Unable to read XML file.'), 'error');
  }

}



function mass_translation_import_form_preview($form, &$form_state) {

  // Set final status (to be rendered in the form).
  unset($form_state['import_process']['status']);
  unset($form_state['import_process']['report']);

  $langcode = $form_state['values']['target_language'];
  $xml_upload = $form_state['values']['xml_upload'];

  if ($xml_upload && $xml_file = file_load($xml_upload)) {

    libxml_use_internal_errors(TRUE);
    $xml_content = file_get_contents($xml_file->uri);
    $xml = simplexml_load_string($xml_content, 'SimpleXMLElement', LIBXML_NOCDATA);
    if ($xml === FALSE) {
      // Set error messages for invalid XML format.
      drupal_set_message(t('Unable to parse XML file.'), 'error');
      $errors = libxml_get_errors();
      foreach ($errors as $error) {
        drupal_set_message(t('Error (line @line-number): @message', array('@line-number' => $error->line, '@message' => trim($error->message))), 'error');
      }
      libxml_clear_errors();
    }
    else {

      $dom = dom_import_simplexml($xml)->ownerDocument;
      $module_path = drupal_get_path('module', 'mass_translation');
      $xsd_path = drupal_realpath($module_path . '/includes/mass_translation_xml_schema.xsd');

      if (!$dom->schemaValidate($xsd_path)) {
        // Set error messages for invalid XML schema.
        drupal_set_message(t('XML do not match with XSD schema.'), 'error');
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
          $message = t('Error @errcode (level @level, line @line): @message', array(
            '@errcode' => $error->code,
            '@level' => $error->level,
            '@message' => trim($error->message),
            '@line' => $error->line,
          ));
          drupal_set_message($message, 'error');
        }
        libxml_clear_errors();
      }
      else {

        foreach ($xml as $group => $elements) {
          switch ($group) {
            case 'nodes':
              $group_title = t('Nodes');
              $count = count($elements->node);
              $count_str = format_plural($count, '1 node', '@count nodes');
              break;
            case 'fields':
              $group_title = t('Fields');
              $count = count($elements->field);
              $count_str = format_plural($count, '1 field', '@count fields');
              break;
            case 'blocks':
              $group_title = t('Blocks');
              $count = count($elements->block);
              $count_str = format_plural($count, '1 block', '@count blocks');
              break;
            case 'menus':
              $group_title = t('Menus');
              $count = count($elements->menu);
              $count_str = format_plural($count, '1 menu', '@count menus');
              break;
            case 'views':
              $group_title = t('Views');
              $count = count($elements->view);
              $count_str = format_plural($count, '1 view', '@count views');
              break;
            case 'strings':
              $modules = $themes = array();
              $count_custom = 0;
              foreach ($elements->string as $string) {
                $attributes = $string->attributes();
                $location = explode(':', $attributes['location']);
                if ($location[0] == 'module' && !in_array($location[1], $modules)) {
                  $modules[] = $location[1];
                }
                elseif ($location[0] == 'theme' && !in_array($location[1], $themes)) {
                  $themes[] = $location[1];
                }
                elseif ($location[0] == 'custom') {
                  $count_custom++;
                }
              }
              $count_total = count($elements->string);
              $count_module = count($modules);
              $count_theme = count($themes);

              $str_count = array(
                '@total' => format_plural($count_total, '1 string', '@count strings'),
                '@modules' => format_plural($count_module, '1 module', '@count modules'),
                '@themes' => format_plural($count_theme, '1 theme', '@count themes'),
                '@custom' => format_plural($count_custom, '1 custom string', '@count custom strings'),
              );

              $group_title = t('Strings');
              $count_str = format_string('@total (@modules; @themes; @custom)', $str_count);
              break;
          }

          $status[$group] = array(
            'group' => array('data' => $group_title),
            'count' => array('data' => $count_str),
          );
        }

        // Set final status (to be rendered in the form).
        $form_state['import_process']['status'] = 'preview';
        $form_state['import_process']['report'] = $status;

      }
    }
  }
  else {
    drupal_set_message(t('Unable to read XML file.'), 'error');
  }

  // Force rebuild with next step.
  $form_state['rebuild'] = TRUE;

}