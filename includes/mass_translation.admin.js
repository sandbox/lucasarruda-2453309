(function($){$(document).ready(function(){

  // Trigger label click in corresponding checkbox
  $('table.mass-translation-export-form').find('tr td').click(function(event){
    if ($(event.target).attr('tagName') != 'A' && $(event.target).attr('type') != 'checkbox') {
      var checkbox = $(this).closest('tr').find('td input:checkbox:enabled');
      checkbox.attr('checked', !checkbox.attr('checked'));
      checkbox.triggerHandler('click');
    }
  });



  if ($('#mass-translation-node-filter-controls').length) {

    // Set filter controls.
    var controls = {
      'title'  : $('#edit-nodetable-controls-node-title'),
      'type'   : $('#edit-nodetable-controls-node-type'),
      // 'status' : $('#edit-nodetable-controls-node-status')
    };

    var getFilter = function(filter) {
      return controls[filter].val();
    }

    // Bind the ENTER key in to not submit the form.
    controls.title.keypress(function(event) {
      if (event.which == 13) {
        event.preventDefault();
      }
    });

    // Bind keyup event to title filter field
    var rowsFilteredByTitle;
    controls.title.keyup(function(){
      var filterRows;
      var filterByType = getFilter('type');
      if (filterByType) {
        filterRows = $('.mass-translation-export-form tbody tr.' + filterByType);
      }
      else {
        filterRows = $('.mass-translation-export-form tbody tr');
      }
      var searchKey = $(this).val().toLowerCase();
      var zebraClass, zebraCounter = 0;
      filterRows.each(function(index, element){
        element = $(element);
        var nodeTitle = element.find('td.node-title label').text()
        if (searchKey == '' || nodeTitle.toLowerCase().indexOf(searchKey) > -1) {
          element.show();
          zebraClass = (zebraCounter % 2) ? 'even' : 'odd';
          element.removeClass('odd even');
          element.addClass(zebraClass);
          zebraCounter++;
        }
        else {
          element.hide();
        }
      });
      rowsFilteredByTitle = $('.mass-translation-export-form tbody tr:visible');
    });

    // Bind change event to content type filter field
    controls.type.change(function(){
      var filterRows;
      if (getFilter('title')) {
        filterRows = rowsFilteredByTitle;
      }
      else {
        filterRows = $('.mass-translation-export-form tbody tr');
      }
      var contentType = $(this).val();
      var zebraClass, zebraCounter = 0;
      filterRows.each(function(index, element){
        element = $(element);
        if (contentType == '' || element.hasClass(contentType)) {
          element.show();
          zebraClass = (zebraCounter % 2) ? 'even' : 'odd';
          element.removeClass('odd even');
          element.addClass(zebraClass);
          zebraCounter++;
        }
        else {
          element.hide();
        }
      });
    });


    // Scrolled table effect.
    var tableWrapper = $('.node-table-wrapper');
    var tableInnerWrapper = $('.node-table-inner-wrapper');
    tableInnerWrapper.scroll(function(){
      if (tableInnerWrapper.scrollTop() > 0) {
        tableWrapper.addClass('scrolled');
      }
      else {
        tableWrapper.removeClass('scrolled');

      }
    });
  }


})})(jQuery);