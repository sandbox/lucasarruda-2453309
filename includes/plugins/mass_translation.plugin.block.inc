<?php

/**
 * @file
 * Contains the class plugin for block translation.
 */


/**
 * Main class for Mass Translation plugins.
 */
class MassTranslationContentBlock extends MassTranslationContent {

  /**
   * Implements MassTranslationContent::getData().
   */
  public function getData() {

    // Load custom block
    $block = block_load('block', $this->identifier);
    $block_custom = block_custom_block_get($this->identifier);
    if (!$block || !$block_custom) {
      return FALSE;
    }
    
    // Verify if content can be translated
    $this->mode = $block->i18n_mode;
    if (!$this->isTranslatable()) {
      return FALSE;
    }
    
    // Generate XML structure for block    
    $structure = array(
      '@attributes' => array(
        'module' => 'block',
        'delta' => $block_custom['bid'],
      ),
      'title' => $this->prepareTranslation($block->title),
      'body' => $this->prepareTranslation($block_custom['body']),
    );

    return $structure;
  }

  /**
   * Implements MassTranslationContent::isTranslatable().
   */
  public function isTranslatable() {
    return $this->mode == I18N_MODE_LOCALIZE;
  }

}
