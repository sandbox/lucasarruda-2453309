<?php

/**
 * @file
 * Contains the classes and interfaces for the services and source plugins.
 */


/**
 * Main interface for Mass Translation plugins.
 */
interface MassTranslationContentInterface {

  /**
   * Constructor.
   *
   * @param $identifier
   *   Content identifier.
   */
  public function __construct($identifier);

  /**
   * Returns the content identifier.
   */
  public function getIdentifier();

  /**
   * Returns the content data.
   */
  public function getData();

  /**
   * Returns if the content can be translated.
   */
  public function isTranslatable();

  /**
   * Returns translation prepared structure.
   */
  public function prepareTranslation($string);

}


/**
 * Main class for Mass Translation plugins.
 */
class MassTranslationContent implements MassTranslationContentInterface {

  protected $identifier;

  /**
   * Implements MassTranslationContentInterface::__construct().
   */
  public function __construct($identifier) {
    $this->identifier = $identifier;
  }
  
  /**
   * Implements MassTranslationContentInterface::getIdentifier().
   */
  public function getIdentifier() {
    return $this->identifier;
  }

  /**
   * Implements MassTranslationContentInterface::getData().
   */
  public function getData() {
    return NULL;
  }

  /**
   * Implements MassTranslationContentInterface::isTranslatable().
   */
  public function isTranslatable() {
    return FALSE;
  }

  /**
   * Implements MassTranslationContentInterface::prepareTranslation().
   */
  public function prepareTranslation($string) {
    return array('source' => $string, 'target' => $string);
  }

}


class SimpleXMLExtended extends SimpleXMLElement {

  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this);
    $document = $node->ownerDocument;
    $node->appendChild($document->createCDATASection($cdata_text));
  }

}