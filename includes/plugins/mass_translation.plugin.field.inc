<?php

/**
 * @file
 * Contains the class plugin for view translation.
 */


/**
 * Main class for Mass Translation plugins.
 */
class MassTranslationContentField extends MassTranslationContent {

  /**
   * Implements MassTranslationContent::getData().
   */
  public function getData() {

    // Verify if content can be translated.
    if (!$this->isTranslatable()) {
      return FALSE;
    }

    // Get source strings and location/context attributes.
    $field_config_strings = NULL;
    $field_instances_strings = array();
    $field_info = field_info_field($this->identifier);
    foreach ($field_info['bundles']['node'] as $bundle) {
      $instance = field_info_instance('node', $this->identifier, $bundle);
      // Field settings strings.
      if (!$field_config_strings) {
        $field_object = i18n_object('field', $instance);
        $field_config_strings = $field_object->get_strings(array('empty' => TRUE));
      }
      // Field instance strings.
      $instance_object = i18n_object('field_instance', $instance);
      $field_instances_strings += $instance_object->get_strings(array('empty' => TRUE));
    }
    
    $field_strings = $field_config_strings + $field_instances_strings;

    $strings = array();
    foreach ($field_strings as $string) {
      $strings[]['string'] = array(
        '@attributes' => array(
          'location' => $string->location,
          'context' => $string->context,
        )
      ) + $this->prepareTranslation($string->string);
    }

    if (!count($strings)) {
      return FALSE;
    }

    // Generate XML structure for field
    $structure = array(
      '@attributes' => array(
        'id' => $this->identifier,
      ),
      'strings' => $strings,
    );
    
    return $structure;
  }

  /**
   * Implements MassTranslationContent::isTranslatable().
   */
  public function isTranslatable() {
    return TRUE;
  }

}
