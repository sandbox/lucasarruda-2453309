<?php

/**
 * @file
 * Contains the class plugin for view translation.
 */


/**
 * Main class for Mass Translation plugins.
 */
class MassTranslationContentView extends MassTranslationContent {

  /**
   * Implements MassTranslationContent::getData().
   */
  public function getData() {

    // Verify if content can be translated
    if (!$this->isTranslatable()) {
      return FALSE;
    }
    
    // Get source strings and location/context attributes.
    $view = views_get_view($this->identifier);
    $object = i18n_object('views', $view);
    $strings = $object->get_strings(array('empty' => TRUE));

    foreach ($strings as $string) {
      $strings[]['string'] = array(
        '@attributes' => array(
          'location' => $string->location,
          'context' => $string->context,
        )
      ) + $this->prepareTranslation($string->string);
    }

    if (!count($strings)) {
      return FALSE;
    }

    // Generate XML structure for view
    $structure = array(
      '@attributes' => array(
        'id' => $this->identifier,
      ),
      'strings' => $strings,
    );

    return $structure;
  }

  /**
   * Implements MassTranslationContent::isTranslatable().
   */
  public function isTranslatable() {
    return TRUE;
  }

}
