<?php

/**
 * @file
 * Contains the class plugin for string translation.
 */


/**
 * Main class for Mass Translation plugins.
 */
class MassTranslationContentString extends MassTranslationContent {

  public function __construct($identifier, $type = 'module') {
    $this->identifier = $identifier;
    $this->type = $type;
  }

  /**
   * Implements MassTranslationContent::getData().
   */
  public function getData() {

    if ($this->type == 'custom') {
      return array('string' => $this->getStringStructure($this->identifier, ''));
    }
    
    global $_potx_strings;

    module_load_include('module', 'potx');
    module_load_include('inc', 'potx');
    potx_status('set', POTX_STATUS_SILENT);

    // Load module/themes files.
    $dir = drupal_get_path($this->type, $this->identifier);
    $files = _potx_explore_dir($dir);

    // Collect every string in affected files.
    foreach ($files as $file) {
      _potx_process_file($file);
    }

    // Get strings from global variable.
    $strings = $_potx_strings;

    // Clean up processed strings.
    $_potx_strings = array();

    // Verify if content can be translated
    if (!$this->isTranslatable()) {
      return FALSE;
    }

    // Generate XML structure for string
    $structure = array();
    foreach ($strings as $string => $string_info) {
      foreach ($string_info as $context => $file_info) {

        // Stip out slash escapes.
        $string = stripcslashes($string);

        $plural = NULL;
        // Plural strings have a null byte delimited format.
        if (strpos($string, "\0") !== FALSE) {
          list($string, $plural) = explode("\0", $string);
        }
        
        // Get string structure.
        $structure[]['string'] = $this->getStringStructure($string, $context);

        if (!empty($plural)) {
          // Get string structure of plural form.
          $structure[]['string'] = $this->getStringStructure($plural, $context);
        }
      }
    }

    return $structure;
  }

  /**
   * Implements MassTranslationContent::isTranslatable().
   */
  public function isTranslatable() {
    return TRUE;
  }

  /**
   * Implements MassTranslationContent::getStringStructure().
   */
  public function getStringStructure($string, $context) {
    $location = $this->type == 'custom' ? 'custom' : $this->type . ':' . $this->identifier;
    
    return array(
      '@attributes' => array(
        'context' => $context,
        'location' => $location,
      )
    ) + $this->prepareTranslation($string);
  }

}
