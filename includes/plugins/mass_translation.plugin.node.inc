<?php

/**
 * @file
 * Contains the class plugin for node translation.
 */


/**
 * Main class for Mass Translation plugins.
 */
class MassTranslationContentNode extends MassTranslationContent {

  private $files;

  /**
   * Implements MassTranslationContent::getData().
   */
  public function getData() {

    // Load node
    $node = node_load($this->identifier);
    if (!$node) {
      return FALSE;
    }

    // Verify if content can be translated
    $this->type = $node->type;
    if (!$this->isTranslatable()) {
      return FALSE;
    }

    // Generate XML structure for block
    $structure = array(
      '@attributes' => array(
        'nid' => $node->nid,
        'type' => $node->type,
        'status' => $node->status,
        'promote' => $node->promote,
        'sticky' => $node->sticky,
        'comment' => $node->comment,
        'format' => (isset($node->body['und']['0']['format'])) ? $node->body['und']['0']['format'] : 'full_html',
      ),
      'title' => $this->prepareTranslation($node->title),
      'path' => $this->prepareTranslation(drupal_get_path_alias('node/' . $node->nid)),
    );

    $fields = array();
    // Fetch fields
    foreach (field_info_instances('node', $node->type) as $field_name => $field_instance) {

      $info = field_info_field($field_name);
      $field = array();
      $field['@attributes'] = array(
        'name' => $field_name,
        'module' => $info['module'],
        'type' => $info['type'],
      );

      $field_data = $node->$field_name;

      if (isset($field_data[LANGUAGE_NONE])) {
        foreach ($field_data[LANGUAGE_NONE] as $key => $value) {
          // Get main field value
          if (isset($value['value'])) {
            $field[$key]['data']['value'] = $this->prepareTranslation($value['value']);
          }

          // Special fields have more values available
          if ($info['module'] == 'text' && $info['type'] == 'text_with_summary') {
            $field[$key]['data']['summary'] = $this->prepareTranslation($value['summary']);
          }
          if ($info['module'] == 'node_reference' && $info['type'] == 'node_reference') {
            $field[$key]['data']['nid'] = $this->prepareTranslation($value['nid']);
          }
          elseif ($info['module'] == 'date' && $info['type'] == 'datetime') {
            if (isset($field[$key]['data']['value2'])) {
              $field[$key]['data']['value2'] = $this->prepareTranslation($value['value2']);
            }
            if (isset($field[$key]['data']['timezone'])) {
              $field[$key]['data']['timezone'] = $this->prepareTranslation($value['timezone']);
            }
          }
          elseif ($info['module'] == 'file' && $info['type'] == 'file') {
            $zip_path = $this->getZipFilePath($field_name, $value['filename']);
            $field[$key]['data']['uri'] = $this->prepareTranslation($zip_path);
            $field[$key]['data']['description'] = $this->prepareTranslation($value['description']);
            $this->storeFile($field_name, $key, $value['uri'], $zip_path);
          }
          elseif ($info['module'] == 'image' && $info['type'] == 'image') {
            $zip_path = $this->getZipFilePath($field_name, $value['filename']);
            $field[$key]['data']['uri'] = $this->prepareTranslation($zip_path);
            $field[$key]['data']['alt'] = $this->prepareTranslation($value['alt']);
            $field[$key]['data']['title'] = $this->prepareTranslation($value['title']);
            $this->storeFile($field_name, $key, $value['uri'], $zip_path);
          }
        }
        
        if (isset($field_data[LANGUAGE_NONE]['0']['format'])) {
          $field['@attributes'] += array(
            'format' => $field_data[LANGUAGE_NONE]['0']['format']
          );
        }
      }

      if (count($field) > 1) {
        $fields[]['field'] = $field;
      }

    }

    if (count($fields)) {
      $structure['fields'] = $fields;
    }

    return $structure;
  }

  /**
   * Implements MassTranslationContent::isTranslatable().
   */
  public function isTranslatable() {
    module_load_include('module', 'i18n_node');
    return i18n_node_type_enabled($this->type);
  }

  /**
   * Implements MassTranslationContentNode::storeFile().
   */
  private function storeFile($field, $index, $uri, $zip_path) {
    $this->files[$field][$index]['file_uri'] = $uri;
    $this->files[$field][$index]['zip_path'] = $zip_path;
  }

  /**
   * Implements MassTranslationContentNode::getZipFilePath().
   */
  private function getZipFilePath($field, $filename) {
    $path_parts = array(
      $this->type,
      $field,
      $this->identifier,
      $filename,
    );
    return implode('/', $path_parts);
  }

  /**
   * Implements MassTranslationContentNode::getFiles().
   */
  public function getFiles() {
    return $this->files;
  }
}
