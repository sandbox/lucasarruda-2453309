<?php

/**
 * @file
 * Contains the class plugin for menu translation.
 */


/**
 * Main class for Mass Translation plugins.
 */
class MassTranslationContentMenu extends MassTranslationContent {

  /**
   * Implements MassTranslationContent::getData().
   */
  public function getData() {

    // Load menu
    $menu = menu_tree_all_data($this->identifier);
    if (!$menu) {
      return FALSE;
    }

    // Verify if content can be translated
    if (!$this->isTranslatable()) {
      return FALSE;
    }

    $structure = array(
      '@attributes' => array(
        'name' => $this->identifier,
      ),
      'items' => $this->getMenuTree($menu),
    );

    return $structure;
  }

  /**
   * Implements MassTranslationContent::isTranslatable().
   */
  public function isTranslatable() {
    $result = db_select('menu_custom', 'm')
      ->fields('m', array('language', 'i18n_mode'))
      ->condition('menu_name', $this->identifier, '=')
      ->execute();
    $record = $result->fetchAssoc();
    return $record['i18n_mode'] == I18N_MODE_MULTIPLE && $record['language'] == LANGUAGE_NONE;
  }

  /**
   * Implements MassTranslationContentMenu::getMenuTree().
   * Recursive function to generate entire menu structure.
   */
  private function getMenuTree($menu) {

    // Get site's default language
    $language_default = language_default()->language;

    $structure = array();
    foreach ($menu as $key => $menu_item) {

      // Menu item should be in default language
      if ($menu_item['link']['language'] != $language_default) {
        continue;
      }

      $children = array();

      // Generate XML structure for menu
      $menu_structure = array(
        '@attributes' => array(
          'id' => $menu_item['link']['mlid'],
          'parent' => $menu_item['link']['plid'],
          'expanded' => $menu_item['link']['expanded'],
          'hidden' => $menu_item['link']['hidden'],
        ),
        'title' => $this->prepareTranslation($menu_item['link']['link_title']),
        'path' => $this->prepareTranslation($menu_item['link']['link_path']),
      );

      if (isset($menu_item['link']['options']['attributes'])) {
        $menu_attributes = $menu_item['link']['options']['attributes'];

        foreach ($menu_attributes as $attribute => $value) {
          if ($attribute == 'class') {
            $value = implode(' ', $value);
          }
          $menu_structure['attributes'][$attribute] = $this->prepareTranslation($value);
        }
      }

      // In case of subitems, call the function recursively
      if (is_array($menu_item['below']) && count($menu_item['below'])) {
        $children = $this->getMenuTree($menu_item['below']);
      }

      $structure[]['item'] = $menu_structure;
      if (count($children)) {
        $structure[] = $children;
      }
    }
    return $structure;
  }

}
