Mass Translation module
----------------------------

ABOUT:
This module allows the Site Administrator to easily export contents for 
translation purposes. The exported XML file containing all selected contents
would be translated then imported again by the module, so the new language
will be ready to be used.

